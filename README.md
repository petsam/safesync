# Safesync
Safesync wants to be yet another Archlinux Repositories Administrator, for more advanced usage, especially useful for experts.

## What can safesync do?
* Creates a pacman mirrorlist from known providers' repositories
* Prompts for custom countries selection (with a menu, when `rofi` is installed)
* Save the new mirrorlist file at the default system path (`/etc/pacman.d/`), or in the invoking user's local respective folder (`$HOME/.local/share/safesync/mirrorlists/`), or in a custom path.
* Rate the servers of a given mirrorlist file and sort them by speed.
* Adds a file heading with info about the creation and modification time, as well as the command and parameters that were used to create/modify the list.

## What can safesync do that is different or better than rankmirrors, or reflector?
* It creates a new mirrorlist and sorts the servers, from known providers' repositories, using the upstream published url, or a .pacnew file. Currently, there is information for Archlinux, Archlinux ARM, EndeavourOS, Chaotic-AUR, Arcolinux, while it is easily extendable for other repositories.
* It is compatible with Arch Linux ARM.
* It also checks any servers Included in pacman.conf
* NEW: Toggle individual servers enabled/disabled, selecting with rofi
* Better speed ranking, in cases where the mirrorlist serves more than one repository (i.e. `extra` or `community` for Archlinux mirrorlist), as it can download a larger file, depending on the given repository, using `<$repo>.files` of the selected `$repo`.
* It does better speed ranking, as it subtracts the connection time from the total download time, leading to a more accurate actual download speed, especially useful on secure protocols (HTTPS).
* It can check remote repo timestamp and disable servers that are synced before the local repository DB timestamp.
* It saves activity logs and debug info in temporary log files, that can be reviewed, for better understanding of servers behavior, or for bug tracking investigation.
* It can print normal, extra, or no activity information during runtime usage.
* It uses normal user privileges, elevating them only if needed, to save at a system path.
* It can be used in scripted mode, where no check for user privileges is accomplished.
* It does not modify or save system mirrorlists, if pacman lock exists, saving in a user folder instead.
* It keeps backups from previous files, when they are modified.


## Command Help
```
Creates, sorts by speed and changes mirror servers safely, for any defined repository
in archlinux/pacman based Linux systems.

  Usage:
  safesync [-h|--help] [-V|--version]
  safesync [OPTIONS] ACTION [LIST]

  The ACTION should always be after all OPTIONS, followed by optional LIST REPOs and/or FILEs
  Read below for explanation of proper usage

  -h|--help : Show this help message
  -V|--version : Print program name and version


  ACTION
  -i |--init : (Re)Initialize the REPO|FILE mirrorlist(s)
        Default: REPO=extra
  -r |--rate : Rate and sort (by fastest) repository servers
        Default: REPO=extra
  -s |--safe : Ensure the active servers are "safe" to sync (synced at a later date than local DB)
        Default: REPO=archlinux (or FILE=/etc/pacman.d/mirrorlist)
  -g |--toggle : Toggle selected servers in a mirrorlist from enabled to disabled and vice versa
        Default: REPO=archlinux (or FILE=/etc/pacman.d/mirrorlist). It requires rofi

  LIST
  A LIST of repositories and/or mirrorlist files to use with ACTION, in this form: [REPO|FILE [REPO|FILE] ...]
  You can mix REPOs and FILEs, if you wish to.
  If LIST is ommited, a default is used. All repositories that use the same mirrorlist,
  are checked and tested, except when using --toggle.

  REPO repository must be defined in pacman.conf. "all" acts on all defined repositories.
  For mirrorlists that serve more than one REPO, you can use any of the known repo names.
  The reason to use a repo instead of a mirrorlist file is the size of the file that will be downloaded.
  For example, with Archlinux "extra" REPO, a larger file is downloaded during speed ranking, producing
  more accurate real world results, than using "core".
  For convenience, we include aliases for known providers, which then use a default REPO per repository.
  "arch" and "archlinux" is equivalent to REPO=extra.
  "archarm" and "archlinuxarm" is equivalent to REPO=extra.
  "arco" and "arcolinux" is equivalent to REPO=arcolinux_repo.
  "chaotic" is equivalent to REPO=chaotic-aur.

  FILE can be absolute, or relative. When a FILE is given, the file name (basename) is used as REPO name.
  For example, when the FILE is a valid REPO name in the current directory, like with

safesync -i extra

  will first check for an existing file "$PWD/extra".
  If the file is found, it will use the entry as a mirrorlist file with REPO="extra" .
  If the file is not found, it will then use the entry as a REPO. So, when the FILE is absolute,
  or in a relative folder, it cannot be a valid REPO name and thus, it will be skipped.

  OPTIONS
  -p |--pacnew : Use pacnew files with --init, instead of fetching upstream list
  -c |--conf FILE : Use custom configuration file FILE, instead of the default
        (Default: FILE=/etc/pacman.conf)
  -NUM |--enable [NUM] : Enable top/first NUM servers in REPO mirrorlist
        (Default: NUM=3)
  -t |--target [TARGET] : Save the output list to TARGET
        (Default: prompt for user input. See bellow for more details)
  --no-conf-servers : Do not check for included Servers directly in pacman.conf
        WARNING: There are usually no Servers in pacman.conf, but it is UNSAFE to use it, in case there are
  --no-safe-test : Do not check servers for sync status, with --init and --rate
  --all-countries : With --init, use all countries in the mirrorlist (do not prompt for selection)
        WARNING: Avoid using all countries with Archlinux repos. There are too many!!
  -x| --scripted : Run without prompting for user input, using parameters, or the defaults
  -q |--quiet : Do not show standard info messages
  -v |--verbose|--debug : Show debug info messages. Used for troubleshooting and bug reports

  TARGET is a PATH or one of the reserved words "system", "local" or "print".
  If "system", file is copied to the pacman.conf defined mirrorlist path.
  If "local", file is copied to safesync local data path ($HOME/.local/share/safesync/mirrorlists).
  If "print", file is printed to terminal output.
  If TARGET is empty, it defaults to $PWD (current working directory/path).
  If --target is not used, it prompts for target, unless --scripted is used which defaults to "print".

  To list all currently configured repositories, run: pacman-conf -l
  If rofi is installed, it will be used for countries selection with --init, and --toggle, or else
  a bash based selection is used.
```

## TODO list
* Create configuration file for custom defaults and settings.
* Add parameters or configuration file options on --verbose, --prefer-secure-protocol, --max-timeout
* Use rsync for check and download, to make it rsync compatible.
* Add URL list testing with --rate
* Add pacman sync functionality, that could replace the standard update command (pacman -Syu)
* Set a lock for system save actions, until exit.
* Set trap(s) for errors to clear temp folder and lock.


